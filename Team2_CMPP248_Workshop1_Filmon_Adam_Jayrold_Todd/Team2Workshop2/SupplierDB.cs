﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop_Team_2
{
    public static class SupplierDB
    {
        public static List<Supplier> GetAllSuppliers()
        {
            List<Supplier> suppliers = new List<Supplier>();
            SqlConnection connection = TravelExpertsDBConnection.GetConnection();
            string selectStatement = "SELECT * FROM Suppliers";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            try
            {
                connection.Open();
                SqlDataReader selectReader = selectCommand.ExecuteReader();
                while (selectReader.Read())
                {
                    Supplier supplier = new Supplier();
                    supplier.SupplierId = Convert.ToInt32(selectReader["SupplierId"]);
                    supplier.SupName = selectReader["SupName"].ToString();

                    suppliers.Add(supplier);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return suppliers;
        }

        public static List<Supplier> GetSuppliersByProductId(int productId)
        {
            List<Supplier> suppliers = new List<Supplier>();
            SqlConnection connection = TravelExpertsDBConnection.GetConnection();
            string selectStatement = "SELECT s.SupName, s.SupplierId FROM Products_Suppliers ps, Suppliers s WHERE ps.SupplierId = s.SupplierId AND ps.ProductId=" + productId;
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            try
            {
                connection.Open();
                SqlDataReader selectReader = selectCommand.ExecuteReader();
                while (selectReader.Read())
                {
                    Supplier supplier = new Supplier();
                    supplier.SupplierId = Convert.ToInt32(selectReader["SupplierId"]);
                    supplier.SupName = selectReader["SupName"].ToString();

                    suppliers.Add(supplier);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return suppliers;
        }
    }
}