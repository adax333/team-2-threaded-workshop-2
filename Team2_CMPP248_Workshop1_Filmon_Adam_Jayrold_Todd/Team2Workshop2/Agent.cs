﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop_Team_2
{
    public class Agent
    {
        /*
         * Purpose : This class stores all information about any Agents that work with Travel Experts
         * Author: Filmon, Adam, Jayrold, Todd - NOte: we all worked on this together as a group.
         * Date: June 29-2015
         */

        //Private variables
        private int agentId, agencyId;

        private string agtFirstName, agtMiddleInitial, agtLastName, agtBusPhone, agtEmail, agtPosition;

        //Public methods (getters/setters)
        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }

        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }

        //Class Constructor
        public Agent(int id, string fname, string middleinitial, string lname, string bphone, string email, string position, int agencyid)
        {
            agentId = id;
            agencyId = agencyid;
            agtFirstName = fname;
            agtMiddleInitial = middleinitial;
            agtLastName = lname;
            agtBusPhone = bphone;
            agtEmail = email;
            agtPosition = position;
        }

        //Override To string to disply the required information
        public override string ToString()
        {
            return "Agent ID: " + agentId + "\nFirst Name: " + agtFirstName + "\nLastName: " + agtLastName + "\nEmail: " + agtEmail + "\nPosition: " + agtPosition + "\nBussiness Phone: " + agtBusPhone + "\nAgency ID: " + agencyId;
        }
    }
}