﻿namespace Team2Workshop2
{
    partial class AddModifyPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtPackageCommision = new System.Windows.Forms.TextBox();
            this.txtPackageBasePrice = new System.Windows.Forms.TextBox();
            this.txtPackageEndDate = new System.Windows.Forms.TextBox();
            this.txtPackageStartDate = new System.Windows.Forms.TextBox();
            this.txtPackageDesc = new System.Windows.Forms.TextBox();
            this.txtPackageName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboProductType = new System.Windows.Forms.ComboBox();
            this.comboSupplierName = new System.Windows.Forms.ComboBox();
            this.productBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(60, 345);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(99, 42);
            this.btnAccept.TabIndex = 8;
            this.btnAccept.Text = "Accept";
            this.btnAccept.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(635, 345);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(99, 42);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // txtPackageCommision
            // 
            this.txtPackageCommision.Location = new System.Drawing.Point(178, 224);
            this.txtPackageCommision.Name = "txtPackageCommision";
            this.txtPackageCommision.Size = new System.Drawing.Size(496, 22);
            this.txtPackageCommision.TabIndex = 36;
            // 
            // txtPackageBasePrice
            // 
            this.txtPackageBasePrice.Location = new System.Drawing.Point(178, 190);
            this.txtPackageBasePrice.Name = "txtPackageBasePrice";
            this.txtPackageBasePrice.Size = new System.Drawing.Size(496, 22);
            this.txtPackageBasePrice.TabIndex = 35;
            // 
            // txtPackageEndDate
            // 
            this.txtPackageEndDate.Location = new System.Drawing.Point(178, 155);
            this.txtPackageEndDate.Name = "txtPackageEndDate";
            this.txtPackageEndDate.Size = new System.Drawing.Size(496, 22);
            this.txtPackageEndDate.TabIndex = 34;
            // 
            // txtPackageStartDate
            // 
            this.txtPackageStartDate.Location = new System.Drawing.Point(178, 124);
            this.txtPackageStartDate.Name = "txtPackageStartDate";
            this.txtPackageStartDate.Size = new System.Drawing.Size(496, 22);
            this.txtPackageStartDate.TabIndex = 33;
            // 
            // txtPackageDesc
            // 
            this.txtPackageDesc.Location = new System.Drawing.Point(178, 92);
            this.txtPackageDesc.Name = "txtPackageDesc";
            this.txtPackageDesc.Size = new System.Drawing.Size(496, 22);
            this.txtPackageDesc.TabIndex = 32;
            // 
            // txtPackageName
            // 
            this.txtPackageName.Location = new System.Drawing.Point(178, 58);
            this.txtPackageName.Name = "txtPackageName";
            this.txtPackageName.Size = new System.Drawing.Size(496, 22);
            this.txtPackageName.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 17);
            this.label7.TabIndex = 29;
            this.label7.Text = "Packages Commision:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 17);
            this.label6.TabIndex = 28;
            this.label6.Text = "Packages Base Price:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 17);
            this.label5.TabIndex = 27;
            this.label5.Text = "Packages End Date:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 17);
            this.label4.TabIndex = 26;
            this.label4.Text = "Packages Start Date:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 17);
            this.label3.TabIndex = 25;
            this.label3.Text = "Packages Description:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 24;
            this.label2.Text = "Package Name:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(75, 265);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 17);
            this.label8.TabIndex = 37;
            this.label8.Text = "Product Type:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(67, 303);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 17);
            this.label9.TabIndex = 39;
            this.label9.Text = "Supplier Name:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboProductType
            // 
            this.comboProductType.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.productBindingSource2, "ProdName", true));
            this.comboProductType.FormattingEnabled = true;
            this.comboProductType.Location = new System.Drawing.Point(178, 262);
            this.comboProductType.Name = "comboProductType";
            this.comboProductType.Size = new System.Drawing.Size(495, 24);
            this.comboProductType.TabIndex = 40;
            this.comboProductType.SelectedIndexChanged += new System.EventHandler(this.comboProductType_SelectedIndexChanged);
            // 
            // comboSupplierName
            // 
            this.comboSupplierName.FormattingEnabled = true;
            this.comboSupplierName.Location = new System.Drawing.Point(178, 300);
            this.comboSupplierName.Name = "comboSupplierName";
            this.comboSupplierName.Size = new System.Drawing.Size(495, 24);
            this.comboSupplierName.TabIndex = 41;
            // 
            // productBindingSource2
            // 
            this.productBindingSource2.DataSource = typeof(Workshop_Team_2.Product);
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataSource = typeof(Workshop_Team_2.Product);
            // 
            // productBindingSource1
            // 
            this.productBindingSource1.DataSource = typeof(Workshop_Team_2.Product);
            // 
            // AddModifyPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 445);
            this.Controls.Add(this.comboSupplierName);
            this.Controls.Add(this.comboProductType);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtPackageCommision);
            this.Controls.Add(this.txtPackageBasePrice);
            this.Controls.Add(this.txtPackageEndDate);
            this.Controls.Add(this.txtPackageStartDate);
            this.Controls.Add(this.txtPackageDesc);
            this.Controls.Add(this.txtPackageName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.Name = "AddModifyPackage";
            this.Text = "AddModifyProduct";
            this.Load += new System.EventHandler(this.AddModifyPackage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtPackageCommision;
        private System.Windows.Forms.TextBox txtPackageBasePrice;
        private System.Windows.Forms.TextBox txtPackageEndDate;
        private System.Windows.Forms.TextBox txtPackageStartDate;
        private System.Windows.Forms.TextBox txtPackageDesc;
        private System.Windows.Forms.TextBox txtPackageName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboProductType;
        private System.Windows.Forms.BindingSource productBindingSource;
        private System.Windows.Forms.ComboBox comboSupplierName;
        private System.Windows.Forms.BindingSource productBindingSource1;
        private System.Windows.Forms.BindingSource productBindingSource2;
    }
}