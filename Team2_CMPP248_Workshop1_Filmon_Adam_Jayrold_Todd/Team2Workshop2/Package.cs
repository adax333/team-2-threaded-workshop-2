﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team2Workshop2
{
    public class Package
    {
        public Package()
        {
        }

        public int PackageId { get; set; }

        public String PkgName { get; set; }

        public DateTime PkgStartDate { get; set; }

        public DateTime PkgEndDate { get; set; }

        public string PkgDesc { get; set; }

        public double PkgBasePrice { get; set; }

        public double PkgAgencyCommission { get; set; }
    }
}