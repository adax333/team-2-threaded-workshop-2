﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Workshop_Team_2;

namespace Team2Workshop2
{
    public partial class AddModifyPackage : Form
    {
        private List<Product> products;
        private List<Supplier> suppliers;

        public AddModifyPackage()
        {
            InitializeComponent();
        }

        private void AddModifyPackage_Load(object sender, EventArgs e)
        {
            products = ProductDB.GetAllProducts();
            comboProductType.Items.Add(products);
            comboProductType.DataSource = products;
            comboProductType.DisplayMember = "ProdName";
            comboProductType.ValueMember = "ProductId";
        }

        private void comboProductType_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboSupplierName.Text = "";
            int i = Convert.ToInt32(comboProductType.SelectedIndex);
            suppliers = SupplierDB.GetSuppliersByProductId(i);
            //comboSupplierName.Items.Add(suppliers);
            comboSupplierName.DataSource = suppliers;
            comboSupplierName.DisplayMember = "SupName";
            comboSupplierName.ValueMember = "SupplierId";
        }
    }
}