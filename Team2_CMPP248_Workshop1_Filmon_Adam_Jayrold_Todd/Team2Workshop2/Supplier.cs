﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop_Team_2
{
    public class Supplier
    {
        /*
         * Purpose : This class stores all information about any suppliers that supply to Travel Experts
         * Author: Filmon, Adam, Jayrold, Todd - NOte: we all worked on this together as a group.
         * Date: June 29-2015
         */

        //Private variables
        private int supplierId;

        private string supName;

        //Public methods (getters/setters)
        public int SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        public string SupName
        {
            get { return supName; }
            set { supName = value; }
        }

        public Supplier()
        {
        }

        //Class Constructor
        public Supplier(int id, string name)
        {
            supplierId = id;
            supName = name;
        }

        //Override To string to disply the required information
        public override string ToString()
        {
            return "Supplier ID: " + supplierId + "\nSupplier Name: " + supName;
        }
    }
}