﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop_Team_2
{
    public static class TravelExpertsDBConnection
    {
        public static SqlConnection GetConnection()
        {
            string connectionString = "Data Source=localhost\\SqlExpress;Initial Catalog=TravelExperts;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);
            return connection;
        }
    }
}