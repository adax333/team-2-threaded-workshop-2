﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop_Team_2;

namespace Team2Workshop2
{
    public static class PackageDB
    {
        public static List<Package> GetAllPackages()
        {
            List<Package> packages = new List<Package>();
            SqlConnection connection = TravelExpertsDBConnection.GetConnection();
            string selectStatement = "SELECT * FROM Packages";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            try
            {
                connection.Open();
                SqlDataReader selectReader = selectCommand.ExecuteReader();
                while (selectReader.Read())
                {
                    Package package = new Package();
                    package.PackageId = Convert.ToInt32(selectReader["PackageId"]);
                    package.PkgName = selectReader["PkgName"].ToString();
                    package.PkgStartDate = Convert.ToDateTime(selectReader["PkgStartDate"]);
                    package.PkgEndDate = Convert.ToDateTime(selectReader["PkgEndDate"]);
                    package.PkgDesc = selectReader["PkgDesc"].ToString();
                    package.PkgBasePrice = Convert.ToDouble(selectReader["PkgBasePrice"]);
                    packages.Add(package);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }

            return packages;
        }

        public static Package GetPackageById(int packageId)
        {
            SqlConnection connection = TravelExpertsDBConnection.GetConnection();
            string selectStatement = "SELECT * FROM Packages WHERE PackageId=" + packageId;
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            try
            {
                connection.Open();
                SqlDataReader selectReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (selectReader.Read())
                {
                    Package package = new Package();
                    package.PackageId = Convert.ToInt32(selectReader["PackageId"]);
                    package.PkgName = selectReader["PkgName"].ToString();
                    package.PkgStartDate = Convert.ToDateTime(selectReader["PkgStartDate"]);
                    package.PkgEndDate = Convert.ToDateTime(selectReader["PkgEndDate"]);
                    package.PkgDesc = selectReader["PkgDesc"].ToString();
                    package.PkgBasePrice = Convert.ToDouble(selectReader["PkgBasePrice"]);
                    return package;
                }
                else
                {
                    return null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}