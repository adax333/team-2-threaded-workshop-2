﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop_Team_2
{
    public static class ProductDB
    {
        public static List<Product> GetAllProducts()
        {
            List<Product> products = new List<Product>();
            SqlConnection connection = TravelExpertsDBConnection.GetConnection();
            string selectStatement = "SELECT * FROM Products";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            try
            {
                connection.Open();
                SqlDataReader selectReader = selectCommand.ExecuteReader();
                while (selectReader.Read())
                {
                    Product product = new Product();
                    product.ProductId = Convert.ToInt32(selectReader["ProductId"]);
                    product.ProdName = selectReader["ProdName"].ToString();

                    products.Add(product);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return products;
        }

        public static List<Product> GetAllProductsByPackageID(int packageId)
        {
            List<Product> products = new List<Product>();
            SqlConnection connection = TravelExpertsDBConnection.GetConnection();
            string selectStatement = "SELECT p.ProductId, p.ProdName FROM Products p, Products_Suppliers ps, Packages_Products_Suppliers pps WHERE p.ProductId = ps.ProductId and ps.ProductSupplierId = pps.ProductSupplierId and pps.PackageId=" + packageId;
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            try
            {
                connection.Open();
                SqlDataReader selectReader = selectCommand.ExecuteReader();
                while (selectReader.Read())
                {
                    Product product = new Product();
                    product.ProductId = Convert.ToInt32(selectReader["ProductId"]);
                    product.ProdName = selectReader["ProdName"].ToString();

                    products.Add(product);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return products;
        }
    }
}