﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop_Team_2
{
    public class Product
    {
        /*
         * Purpose : This class stores all information about any product types that travel experts sell
         * Author: Filmon, Adam, Jayrold, Todd - NOte: we all worked on this together as a group.
         * Date: June 29-2015
         */

        //Private variables
        private int productId;

        private string prodName;

        //Public methods (getters/setters)
        public int ProductId
        {
            get { return productId; }
            set { productId = value; }
        }

        public string ProdName
        {
            get { return prodName; }
            set { prodName = value; }
        }

        //Class Constructor
        public Product(int id, string name)
        {
            productId = id;
            prodName = name;
        }

        public Product()
        {
        }

        //Override To string to disply the required information
        public override string ToString()
        {
            return "Product ID: " + productId + "\nProduct Name: " + prodName;
        }
    }
}