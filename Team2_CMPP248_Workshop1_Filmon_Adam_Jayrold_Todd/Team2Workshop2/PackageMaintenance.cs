﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Workshop_Team_2;

namespace Team2Workshop2
{
    public partial class PackageMaintenance : Form
    {
        private List<Package> packages = null;
        private Package package;

        public PackageMaintenance()
        {
            InitializeComponent();
        }

        private void ProductMaintenanceForm_Load(object sender, EventArgs e)
        {
        }

        private void btnGetPackage_Click(object sender, EventArgs e)
        {
            try
            {
                package = PackageDB.GetPackageById(Convert.ToInt32(txtPackageId.Text));
                FillPackageTextFields(package);
                productDataGridView.DataSource = ProductDB.GetAllProductsByPackageID(Convert.ToInt32(txtPackageId.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Package Id was not found, please try again", "Package ID Not found");
            }
        }

        private void FillPackageTextFields(Package package)
        {
            txtPackageId.Text = package.PackageId.ToString();
            txtPackageDesc.Text = package.PkgDesc;
            txtPackageBasePrice.Text = package.PkgBasePrice.ToString();
            txtPackageCommision.Text = package.PkgAgencyCommission.ToString();
            txtPackageEndDate.Text = package.PkgEndDate.ToString();
            txtPackageName.Text = package.PkgName;
            txtPackageStartDate.Text = package.PkgStartDate.ToString();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Creates a new instance of the Add/modify form
            AddModifyPackage addProductForm = new AddModifyPackage();
            //Listener to see if we recieve a dialog result from the add/modify form
            DialogResult dialogResult = addProductForm.ShowDialog();
            //if we recieve an "Ok" return, display information
            if (dialogResult == DialogResult.OK)
            {
            }
        }
    }
}